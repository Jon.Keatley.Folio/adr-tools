/*
This module is responsible for a config
    - load template 
        - user provided
        - CWD adr_template/adr.md
        - ~/
    - generate standard ADR template folder - default to CWD unless user provided
    - generate standard ADR template 
*/

use std::{path::PathBuf, str::FromStr, error::Error, io};


pub struct  ADRConfig {
   pub is_new:bool, 
   pub config_path:PathBuf,
}

pub fn get_or_create_config(path:Option<&str>) -> Result<ADRConfig,Box<dyn Error>> {
    

    //build path
    let c_path:PathBuf;
    if path.is_some()
    {
        //check path exists
        println!("Using user provided config");
        c_path = PathBuf::from_str(path.unwrap())?;
        
        if !c_path.exists() || c_path.is_file()
        {
            return Err(Box::new(io::Error::new(io::ErrorKind::NotFound, "Provided config path does not exist or is not a directory")))
        }
    }
    else 
    {
        c_path = std::env::current_dir()?;
    }
    
    //append config directory name
    let config = ADRConfig {
        is_new:true,
        config_path: c_path
    };
    
    
    //check for template
    
    //create template if needed
    
    //return config struct <- need to design this
    
    
    
    return Ok(config)
}

