use crate::config::ADRConfig;

mod cli;
mod config;

/*
ADR tools 
- Create new ADRs
    - title
    - supersede and existing one (optional)
    - template name (optionl)
- list adrs
- list templates
- render adrs

- need to support adr path
- adr template path
- labels?


*/

fn execute_command(args: cli::CLIOptions)
{
    let println_basic_command = |command:&str| println!("Command: {}", command);
    
    let user_config: Option<&str> = match args.config.is_empty(){
        true => None,
        false=> Some(&args.config)
    };
    
    let get_user_config_result = config::get_or_create_config(user_config);
    
    if get_user_config_result.is_err()
    {
        println!("Failed to access config");
        //TODO display error
        return
    }
    
    let user_config:ADRConfig = get_user_config_result.unwrap();
    
    println!("{}", user_config.config_path.display()); 
    
    match args.cmd {
    cli::Command::Add { title, supersede }=> {
        println!("Command: {} [title=\"{}\" supersede=\"{}\"]","Add", title, supersede);
    },
    cli::Command::List => println_basic_command("List"),
    cli::Command::Render => println_basic_command("Render"),
    }

    
}

fn main() {
    let cmd = cli::gather_cli_args();
    
    execute_command(cmd);
}
