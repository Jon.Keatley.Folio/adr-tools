use structopt::StructOpt;

#[derive(StructOpt)]
#[structopt(name="adr-tools", about="A set of tools to take the heavy lifting out of ADRs")]
pub struct CLIOptions {
    #[structopt(short="c", long="config", default_value="", help="Location of configuration folder")]
    pub config:String, 
    #[structopt(subcommand)]
    pub cmd:Command
}

#[derive(StructOpt)]
pub enum Command {
    #[structopt(name="add")]
    Add {
      title: String,
      #[structopt(short="s", long="supersede")]
      supersede: bool  
    },
    #[structopt(name="list")]
    List,
    Render
}

pub fn gather_cli_args() -> CLIOptions {
    CLIOptions::from_args()
}
